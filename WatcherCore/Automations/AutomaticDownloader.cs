﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EonDream.ArloWatcher.WatcherCore.Api.Client;
using EonDream.ArloWatcher.WatcherCore.DownloadManager;

namespace EonDream.ArloWatcher.WatcherCore.Automations {
    public class AutomaticDownloader : IDisposable {
        public string BaseDownloadFolder { get; set; }
        public string RecordingFolderName { get; set; } = "Recordings";
        public string ThumbnailFolderName { get; set; } = "Recordings\\Thumbnails";
        public string PhotoFolderName { get; set; } = "Photos";
        public FileDownloadManager Downloads { get; private set; }
        public bool DownloadThumbnails { get; set; } = true;
        public bool DeleteRecordingsFromArlo { get; set; } = true;
        public bool IsCompleted {
            get {
                return Downloads.IsCompleted;
            }
        }

        private ArloClient arlo;
        private Dictionary<string, ArloRecording> downloadTracker = new Dictionary<string, ArloRecording>();
        private Dictionary<string, int> downloadRetries = new Dictionary<string, int>();

        public AutomaticDownloader(string downloadFolder, ArloClient api) {
            BaseDownloadFolder = downloadFolder;
            arlo = api;
            Downloads = new FileDownloadManager();

            Downloads.FileDownloadSuccessful += DownloadSuccessfulHandler;
            Downloads.FileDownloadFailed += DownloadFailedHandler;
        }

        public async Task StartAutomaticDownload() {
            var metadata = await arlo.GetLibraryInfoAsync();

            foreach (var dt in metadata.AllDates) {
                await DownloadRecordings(dt);
            }
        }

        public async Task DownloadRecordings(DateTime dt) {
            var recordings = await arlo.GetRecordingsAsync(dt);
            foreach (var rec in recordings) {
                DownloadRecording(rec);
            }
        }

        public void DownloadRecording(ArloRecording rec) {
            var filename = GetFilename(rec);
            downloadTracker.Add(filename, rec);
            Downloads.Download(rec.RecordingVideoUrl, filename);

            // download the thumbnail for videos
            if (rec.ContentType.Equals("video/mp4", StringComparison.OrdinalIgnoreCase)) {
                Downloads.Download(rec.RecordingThumbnailUrl, GetFilename(rec, true));
            }
        }

        private async void DownloadSuccessfulHandler(object o, FileDownloadSuccessEventArgs e) {
            if (DeleteRecordingsFromArlo && downloadTracker.ContainsKey(e.Filename)) {
                if (await arlo.DeleteRecording(downloadTracker[e.Filename])) {
                    downloadTracker.Remove(e.Filename);
                }
            }
        }

        private void DownloadFailedHandler(object o, FileDownloadFailedEventArgs e) {
            if (!e.Cancelled) {
                if (downloadRetries.ContainsKey(e.Filename)) {
                    if (downloadRetries[e.Filename]++ > 10) {
                        return;
                    }
                }
                else {
                    downloadRetries.Add(e.Filename, 1);
                }

                Downloads.Download(e.RequestUri, e.Filename);
            }
        }

        private string GetFilename(ArloRecording recording, bool getThumbnailName = false) {
            // add the date folder
            var dateFolder = recording.CreatedAt.ToString("yyyy_MM_dd");
            var resultPath = appendToPath(BaseDownloadFolder, dateFolder);

            // determine the correct path and extension for this file
            string fileTypeFolder = string.Empty;
            string fileTypeExtension = string.Empty;
            if (getThumbnailName) {
                fileTypeFolder = ThumbnailFolderName;
                fileTypeExtension = "jpg";
            }
            else if (recording.ContentType.Equals("video/mp4", StringComparison.OrdinalIgnoreCase)) {
                fileTypeFolder = RecordingFolderName;
                fileTypeExtension = "mp4";
            }
            else if (recording.ContentType.Equals("image/jpg", StringComparison.OrdinalIgnoreCase)) {
                fileTypeFolder = PhotoFolderName;
                fileTypeExtension = "jpg";
            }
            resultPath = appendToPath(resultPath, fileTypeFolder);

            // create the folder if it doesn't exist
            if (!Directory.Exists(resultPath)) {
                Directory.CreateDirectory(resultPath);
            }

            // create the reason tag to use in the filename
            var reasonTag = string.Empty;
            if (recording.Reason.Equals("motionRecord", StringComparison.OrdinalIgnoreCase)) {
                reasonTag = "MOTION";
            }
            else if (recording.Reason.Equals("record", StringComparison.OrdinalIgnoreCase)) {
                reasonTag = "MANUAL";
            }
            else if (recording.Reason.Equals("snapshot", StringComparison.OrdinalIgnoreCase)) {
                reasonTag = "MANUAL";
            }

            // create the filename
            var cameraName = (recording.CaptureCamera?.DeviceName) ?? recording.CaptureCameraId ?? "unk";
            string fileName = $"{ recording.CreatedAt.ToString("HHmmssfff") }-{ cameraName.Replace(" ", "_") }-{ reasonTag }.{ fileTypeExtension }";
            resultPath = appendToPath(resultPath, fileName);

            return resultPath;
        }

        private string appendToPath(string originalPath, string subPath) {
            if (originalPath.EndsWith("\\")) {
                return string.Concat(originalPath, subPath);
            }
            else {
                return string.Concat(originalPath, "\\", subPath);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    Downloads.Dispose();
                    arlo?.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiHttpErrorEventArgs : ArloApiResponseEventArgs {
        public string ErrorMessage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiDeviceProperties {
        public string HwVersion { get; set; }
        public string ModelId { get; set; }
        [JsonProperty("olsonTimeZone")]
        public string TimeZone { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiUserPlan {
        public string PlanId { get; set; }
        public string PlanName { get; set; }
        public string PlanType { get; set; }
        public int MaxBaseStations { get; set; }
        public int MaxCameras { get; set; }
        public int NumPushNotify { get; set; }
        public int MaxSmartModes { get; set; }
        public int MaxAccounts { get; set; }
        public int MaxStorage { get; set; }
        public int GroupNumber { get; set; }
        public string GroupName { get; set; }
        public long CreatedDate { get; set; }
        public long LastModified { get; set; }
        public string ExpiryDate { get; set; }
        public int? DaysLeftForExpiry { get; set; }
        public string BillingDate { get; set; }
        public int Term { get; set; }
        public string PlanAmount { get; set; }
        public string PlansTotalCurrencyAmount { get; set; }
        public string PlanCurrencyAmount { get; set; }
        public bool PlanUpgradable { get; set; }
        public ArloApiUserPlanPreferences UserPreferences { get; set; }
        public int LibraryAccessExpiry { get; set; }
        public int CvrAccessExpiry { get; set; }
        public bool Sharing { get; set; }
        public bool Schedule { get; set; }
        public int SharingExpiry { get; set; }
        public int ScheduleExpiry { get; set; }
        public string Display { get; set; }
        public bool? IsBusiness { get; set; }
        public string PlanCapacity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EonDream.ArloWatcher.WatcherCore.Extensions;
using Newtonsoft.Json;


namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApi : IDisposable {
        public const string applicationName = "ArloWatcher";
        public const string applicationVersion = "0.0.1a";

        #region arlo api urls
        private const string apiUrlBase = "https://arlo.netgear.com/hmsweb/";
        private const string apiUrlDeviceSupport = "devicesupport/v2";
        private const string apiUrlLogin = "login/v2";
        private const string apiUrlLogout = "logout";
        private const string apiUrlPlans = "users/serviceLevel/v2";
        private const string apiUrlDevices = "users/devices";
        private const string apiUrlLibrary = "users/library";
        private const string apiUrlLibraryMetadata = "users/library/metadata/v2";
        private const string apiUrlLibraryDelete = "users/library/recycle";
        #endregion

        public bool IsLoggedIn { get; set; }

        private HttpClientHandler clientHandler;
        private HttpClient client;

        public event EventHandler<ArloApiHttpErrorEventArgs> ApiHttpError;
        public event EventHandler<ArloApiResponseEventArgs> ReceivedResponse;
        public event EventHandler<ArloApiRequestEventArgs> SendingRequest;

        public ArloApi() {
            clientHandler = getDefaultHttpClientHandler();
            client = getDefaultHttpClient(clientHandler);
        }

        public async Task<ArloApiUserLogin> LoginAsync(string loginUsername, string loginPassword) {
            // first visit the supported device page which sets a couple of session cookies
            var r = await GetRequestAsync(apiUrlDeviceSupport);
            r.Dispose();

            var postDataJson = JsonConvert.SerializeObject(new {
                email = loginUsername,
                password = Convert.ToBase64String(Encoding.Default.GetBytes(loginPassword))
            });

            var postContent = createStringContent(postDataJson);
            postContent.Headers.Add("Password-Encoded", "true");

            using (var response = await PostRequestAsync(apiUrlLogin, postContent)) {
                if (!response.IsSuccessStatusCode) {
                    return null;
                }

                var responseData = await ReadResponseAsync<ArloApiUserLogin>(response);

                if (!responseData.Success) {
                    return null;
                }

                client.DefaultRequestHeaders.Add("Authorization", responseData.Data.Token);
                IsLoggedIn = true;
                return responseData.Data;
            }
        }

        public async Task LogoutAsync() {
            if (!IsLoggedIn) {
                return;
            }

            var response = await PutRequestAsync(apiUrlLogout, null);

            if (response.IsSuccessStatusCode) {
                IsLoggedIn = false;

                // reset http client
                client.Dispose();
                clientHandler.Dispose();
                clientHandler = getDefaultHttpClientHandler();
                client = getDefaultHttpClient(clientHandler);
            }
        }

        public async Task<List<ArloApiDevice>> GetDevicesAsync() {
            var response = await GetRequestAsync(apiUrlDevices);

            if (response.IsSuccessStatusCode) {
                var devices = await ReadResponseAsync<List<ArloApiDevice>>(response);
                return devices.Data;
            }
            else {
                return null;
            }
        }

        public async Task<List<ArloApiUserPlan>> GetPlansAsync() {
            var response = await GetRequestAsync(apiUrlPlans);

            if (response.IsSuccessStatusCode) {
                var plans = await ReadResponseAsync<ArloApiServiceLevel>(response);
                return plans.Data.Plans;
            }
            else {
                return null;
            }
        }

        public async Task<List<ArloApiRecording>> GetRecordingsAsync(DateTime fromDate) {
            return await GetRecordingsAsync(fromDate, fromDate);
        }

        public async Task<List<ArloApiRecording>> GetRecordingsAsync(DateTime fromDate, DateTime toDate) {
            var postDataJson = JsonConvert.SerializeObject(new {
                dateFrom = fromDate.ToString("yyyyMMdd"),
                dateTo = toDate.ToString("yyyyMMdd")
            });
            var response = await PostRequestAsync(apiUrlLibrary, postDataJson);
            if (response.IsSuccessStatusCode) {
                var recordings = await ReadResponseAsync<List<ArloApiRecording>>(response);
                return recordings.Data;
            }
            else {
                return null;
            }
        }

        public async Task<ArloApiLibraryMetadata> GetLibraryMetadataAsync() {
            var response = await GetRequestAsync(apiUrlLibraryMetadata);
            if (response.IsSuccessStatusCode) {
                var result = await ReadResponseAsync<ArloApiLibraryMetadata>(response);
                return result.Data;
            }
            else {
                return null;
            }
        }

        public async Task<bool> DeleteRecording(ArloApiRecording recording) {
            return await DeleteRecording(new List<ArloApiRecording>() { recording });
        }
        public async Task<bool> DeleteRecording(List<ArloApiRecording> recordings) {
            var postDataJson = JsonConvert.SerializeObject(new {
                data = recordings.Select(r => new {
                        createdDate = r.CreatedDate,
                        utcCreatedDate = r.UtcCreatedDate,
                        deviceId = r.DeviceId
                    })
            });

            var response = await PostRequestAsync(apiUrlLibraryDelete, postDataJson);
            if (response.IsSuccessStatusCode) {
                return await ReadCommandResponseAsync(response);
            }

            return false;
        }

        private HttpClientHandler getDefaultHttpClientHandler() {
            // create and configure the httpclienthandler object which stores/maintains the cookies used by the connection
            return new HttpClientHandler() {
                UseCookies = true,
                CookieContainer = new CookieContainer(),
                AllowAutoRedirect = true
            };
        }

        private HttpClient getDefaultHttpClient(HttpClientHandler clientHandler) {
            // create and configure the httpclient object used to interact with the web service
            var client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri(apiUrlBase);
            client.DefaultRequestHeaders.Host = "arlo.netgear.com";
            client.DefaultRequestHeaders.Connection.Add("keep-alive");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
            client.DefaultRequestHeaders.Add("Origin", "https://arlo.netgear.com");
            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(applicationName, applicationVersion));
            client.DefaultRequestHeaders.Referrer = new Uri("https://arlo.netgear.com/");
            client.DefaultRequestHeaders.Expect.Clear();

            return client;
        }

        private async Task<HttpResponseMessage> GetRequestAsync(string uri) {
            raiseSendingRequestEvent(uri, "GET", null);
            var response = await client.GetAsync(uri);
            checkResponse(response);
            return response;
        }

        private async Task<HttpResponseMessage> PostRequestAsync(string uri, string postData) {
            return await PostRequestAsync(uri, createStringContent(postData));
        }

        private async Task<HttpResponseMessage> PostRequestAsync(string uri, HttpContent postData) {
            raiseSendingRequestEvent(uri, "POST", postData);
            var response = await client.PostAsync(uri, postData);
            checkResponse(response);
            return response;
        }

        private async Task<HttpResponseMessage> PutRequestAsync(string uri, HttpContent putData) {
            raiseSendingRequestEvent(uri, "PUT", putData);
            var response = await client.PutAsync(uri, putData);
            checkResponse(response);
            return response;
        }

        private void raiseSendingRequestEvent(string uri, string type, HttpContent data) {
            SendingRequest?.Invoke(this, new ArloApiRequestEventArgs() {
                RequestUri = uri,
                RequestType = type
            });
        }

        private StringContent createStringContent(string postData) {
            string encodedPostData = Encoding.UTF8.GetString(Encoding.Default.GetBytes(postData));
            var postContent = new StringContent(encodedPostData, Encoding.UTF8, "application/json");
            return postContent;
        }

        private async Task<ArloApiResponse<T>> ReadResponseAsync<T>(HttpResponseMessage response) {
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ArloApiResponse<T>>(responseString);
        }

        private async Task<bool> ReadCommandResponseAsync(HttpResponseMessage response) {
            var responseString = await response.Content.ReadAsStringAsync();
            var typeDef = new { success = false };
            var result = JsonConvert.DeserializeAnonymousType(responseString, typeDef);
            return result.success;
        }

        private bool checkResponse(HttpResponseMessage response) {
            ReceivedResponse?.Invoke(this, new ArloApiResponseEventArgs() {
                RequestUrl = response.RequestMessage.RequestUri.AbsoluteUri,
                StatusCode = response.StatusCode
            });

            if (!response.IsSuccessStatusCode) {
                ApiHttpError?.Invoke(this, new ArloApiHttpErrorEventArgs() {
                    RequestUrl = response.RequestMessage.RequestUri.AbsoluteUri,
                    StatusCode = response.StatusCode,
                    ErrorMessage = response.ReasonPhrase
                });
            }

            return response.IsSuccessStatusCode;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    client.Dispose();
                    clientHandler.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}

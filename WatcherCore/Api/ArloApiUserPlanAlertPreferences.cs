﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiUserPlanAlertPreferences {
        public bool StorageAlert { get; set; }
        public bool LowBatteryAlert { get; set; }
        public bool PushNotificationAlert { get; set; }
        public int PushNotificationCount { get; set; }
    }
}

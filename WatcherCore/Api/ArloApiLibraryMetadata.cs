﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    /* this awkward type is 5 string-keyed dictionaries with the final nested value being an integer
     * 
     * how to make a library metadata layer cake:
     *      1. Date as string YYYYMMDD
     *      2. The UniqueId of every device with a video recording on that date
     *      3. The reason for the recording [manual/motion]
     *      4. Favorited or not [nonFavorite/favorite]
     *      5. Always "Other" ... never seen any other value ...
     *          + This contains the count of videos broken down by the categories above.
     */
    using ArloApiLibraryMetadataType = Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, int>>>>>;

    public class ArloApiLibraryMetadata {
        public ArloApiLibraryMetadataType Metadata { get; set; }
    }
}

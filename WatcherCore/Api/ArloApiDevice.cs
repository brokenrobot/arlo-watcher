﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiDevice {
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public int DisplayOrder { get; set; }
        [JsonProperty("interfaceSchemaVer")]
        public int InterfaceSchemaVersion { get; set; }
        public long LastModified { get; set; }
        public int MediaObjectCount { get; set; }
        public string ModelId { get; set; }
        public ArloApiDeviceOwner Owner;
        public ArloApiDeviceProperties Properties;
        public string State { get; set; }
        public string UniqueId { get; set; }
        public string UserId { get; set; }
        public string UserRole { get; set; }
        public string xCloudId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiRequestEventArgs : EventArgs {
        public string RequestUri { get; set; }
        public string RequestType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiUserLogin {
        public string AccountStatus { get; set; }
        public bool Arlo { get; set; }
        public string Authenticated { get; set; }
        public string CountryCode { get; set; }
        public long DateCreated { get; set; }
        public string Email { get; set; }
        public string PaymentId { get; set; }
        public bool PolicyUpdate { get; set; }
        public string SerialNumber { get; set; }
        public bool ToUpdate { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public bool ValidEmail { get; set; }
    }
}

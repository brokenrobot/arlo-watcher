﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiUserPlanStoragePreferences {
        public bool Enabled { get; set; }
        public bool AutoDelete { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiResponseEventArgs : EventArgs {
        public string RequestUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}

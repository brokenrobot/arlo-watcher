﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiRecording {
        public string ContentType { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CurrentState { get; set; }
        public string DeviceId { get; set; }
        public bool Donated { get; set; }
        public long LastModified { get; set; }
        public long LocalCreatedDate { get; set; }
        public string MediaDuration { get; set; }
        public int MediaDurationSecond { get; set; }
        public string Name { get; set; }
        public string OwnerId { get; set; }
        public string PresignedContentUrl { get; set; }
        public string PresignedThumbnailUrl { get; set; }
        public string Reason { get; set; }
        public string TimeZone { get; set; }
        public string UniqueId { get; set; }
        public long UtcCreatedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    class ArloLibraryMetadata {
        public string LibraryDateLabel { get; set; }
        public DateTime LibraryDate { get; set; }
        public string DeviceUniqueId { get; set; }
        public string Reason { get; set; }
        public string Favorited { get; set; }
        public int Count { get; set; }
        public string Vestigial { get; set; }
    }
}

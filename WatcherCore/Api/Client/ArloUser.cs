﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EonDream.ArloWatcher.WatcherCore.Api;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    public class ArloUser {
        private ArloApiUserLogin loginInfo;

        public string Email {
            get {
                return loginInfo.Email;
            }
        }

        public string UserId {
            get {
                return loginInfo.UserId;
            }
        }

        internal ArloUser(ArloApiUserLogin login) {
            loginInfo = login;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EonDream.ArloWatcher.WatcherCore.Extensions;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    public class ArloDevice {
        private ArloApiDevice device;

        public string DeviceId {
            get {
                return device.DeviceId;
            }
        }

        public string DeviceName {
            get {
                return device.DeviceName;
            }
        }

        public string DeviceType {
            get {
                return device.DeviceType;
            }
        }

        public string OwnerName {
            get {
                if (!string.IsNullOrEmpty(device.Owner.FirstName) && !string.IsNullOrEmpty(device.Owner.LastName)) {
                    return $"{ device.Owner.FirstName } { device.Owner.LastName }";
                }

                if (!string.IsNullOrEmpty(device.Owner.FirstName)) {
                    return device.Owner.FirstName;
                }
                else {
                    return device.Owner.LastName;
                }
            }
        }

        public string HardwareVersion {
            get {
                return device.Properties.HwVersion;
            }
        }

        public string ModelId {
            get {
                return device.ModelId;
            }
        }

        public DateTime LastModified {
            get {
                return device.LastModified.ConvertFromEpochTime();
            }
        }

        internal ArloDevice(ArloApiDevice thisDevice) {
            device = thisDevice;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    public class ArloClient : IDisposable {
        public ArloApi Api { get; private set; }
        public ArloUser User { get; private set; }

        public bool IsLoggedIn {
            get {
                return Api.IsLoggedIn;
            }
        }

        public ArloClient() : this(new ArloApi()) { }

        public ArloClient(ArloApi thisApi) {
            Api = thisApi;

            // configure arlo api object
            Api.ReceivedResponse += ApiResponseReceivedHandler;
            Api.SendingRequest += ApiRequestSentHandler;
            Api.ApiHttpError += ApiErrorHandler;
        }

        public async Task<bool> LoginAsync(string Username, string Password) {
            var response = await Api.LoginAsync(Username, Password);

            if (response == null) {
                return false;
            }

            User = new ArloUser(response);
            return true;
        }

        public async Task LogoutAsync() {
            await Api.LogoutAsync();
            User = null;
        }

        public async Task<List<ArloRecording>> GetRecordingsAsync(DateTime dateFrom) {
            return await GetRecordingsAsync(dateFrom, dateFrom);
        }

        public async Task<List<ArloRecording>> GetRecordingsAsync(DateTime dateFrom, DateTime dateTo) {
            var devices = await GetDevicesAsync();
            var recordings = await Api.GetRecordingsAsync(dateFrom, dateTo);

            var result = new List<ArloRecording>();
            foreach (var r in recordings) {
                var rec = new ArloRecording(r);
                var captureCam = devices.Where(d => d.DeviceId.Equals(r.DeviceId, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                rec.SetCaptureCamera(captureCam);
                result.Add(rec);
            }
            return result;
        }

        private IEnumerable<ArloDevice> _devices;
        public async Task<List<ArloDevice>> GetDevicesAsync(bool forceUpdate = false) {
            if (forceUpdate || _devices == null) {
                var apiDevices = await Api.GetDevicesAsync();
                _devices = apiDevices.Select(d => new ArloDevice(d));
            }

            return _devices.ToList();
        }

        public async Task<ArloLibrary> GetLibraryInfoAsync() {
            var metadata = await Api.GetLibraryMetadataAsync();
            return new ArloLibrary(metadata);
        }

        public async Task<bool> DeleteRecording(ArloRecording recording) {
            return await Api.DeleteRecording(recording);
        }

        private void ApiResponseReceivedHandler(object sender, ArloApiResponseEventArgs responseEvent) { }

        private void ApiRequestSentHandler(object sender, ArloApiRequestEventArgs requestEvent) { }

        private void ApiErrorHandler(object sender, ArloApiHttpErrorEventArgs errorEvent) { }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    Api.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EonDream.ArloWatcher.WatcherCore.Extensions;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    public class ArloRecording {
        private ArloApiRecording recording;

        public string ContentType {
            get {
                return recording.ContentType;
            }
        }

        public string CreatedBy {
            get {
                return recording.CreatedBy;
            }
        }

        public DateTime CreatedAt {
            get {
                return recording.LocalCreatedDate.ConvertFromEpochTime();
            }
        }

        public string CurrentState {
            get {
                return recording.CurrentState;
            }
        }

        public string RecordingVideoUrl {
            get {
                return recording.PresignedContentUrl;
            }
        }

        public string RecordingThumbnailUrl {
            get {
                return recording.PresignedThumbnailUrl;
            }
        }

        public string Reason {
            get {
                return recording.Reason;
            }
        }

        public string UniqueId {
            get {
                return recording.UniqueId;
            }
        }

        public string CaptureCameraId {
            get {
                return recording.CreatedBy;
            }
        }

        public string InternalName {
            get {
                return recording.Name;
            }
        }

        public int MediaDurationSeconds {
            get {
                return recording.MediaDurationSecond;
            }
        }

        public string MediaDurationLabel {
            get {
                return recording.MediaDuration;
            }
        }

        public ArloDevice CaptureCamera { get; private set; }

        internal ArloRecording(ArloApiRecording thisRecording) {
            recording = thisRecording;
        }

        internal void SetCaptureCamera(ArloDevice camera) {
            CaptureCamera = camera;
        }

        // doing this for now even though it is a bit not best practice... it's just so cool!
        public static implicit operator ArloApiRecording(ArloRecording recording) {
            return recording.recording;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api.Client {
    public class ArloLibrary {
        private IEnumerable<ArloLibraryMetadata> metadata;

        public IEnumerable<DateTime> AllDates {
            get {
                return metadata.Select(d => d.LibraryDate).Distinct();
            }
        }

        public DateTime EarliestDate {
            get {
                return metadata.Min(d => d.LibraryDate);
            }
        }

        public DateTime LatestDate {
            get {
                return metadata.Max(d => d.LibraryDate);
            }
        }

        public int TotalRecordingCount {
            get {
                return metadata.Select(d => d.Count).Sum();
            }
        }

        internal ArloLibrary(ArloApiLibraryMetadata thisMetadata) {
            // flatten out the nested dictionaries into a more managable object
            metadata = from recDates in thisMetadata.Metadata
                       from devices in recDates.Value
                       from reasons in devices.Value
                       from faved in reasons.Value
                       from unk in faved.Value
                       select new ArloLibraryMetadata() {
                           LibraryDateLabel = recDates.Key,
                           LibraryDate = ConvertFromLabel(recDates.Key),
                           DeviceUniqueId = devices.Key,
                           Reason = reasons.Key,
                           Favorited = faved.Key,
                           Vestigial = unk.Key,
                           Count = unk.Value
                       };
        }

        private DateTime ConvertFromLabel(string dateLabel) {
            var y = int.Parse(dateLabel.Substring(0, 4));
            var m = int.Parse(dateLabel.Substring(4, 2));
            var d = int.Parse(dateLabel.Substring(6, 2));

            return new DateTime(y, m, d);
        }
    }
}

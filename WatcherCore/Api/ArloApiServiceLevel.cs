﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    class ArloApiServiceLevel {
        public List<ArloApiUserPlan> Plans { get; set; }
        public Object Discovery { get; set; }
    }
}

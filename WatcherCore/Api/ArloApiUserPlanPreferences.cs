﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    public class ArloApiUserPlanPreferences {
        public ArloApiUserPlanStoragePreferences Storage { get; set; }
        public ArloApiUserPlanAlertPreferences Alerts { get; set; }
    }
}

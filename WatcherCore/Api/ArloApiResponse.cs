﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Api {
    class ArloApiResponse<T> {
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.DownloadManager {
    public class FileDownloadTransfer {
        public Uri FileUri { get; set; }
        public string FileName { get; set; }
        public long FileSizeBytes { get; set; }
        public long DownloadedBytes { get; set; }
        public int DownloadedPercent { get; set; }
        public FileDownloadState State { get; set; }
        public DateTime RequestedDate { get; private set; } = DateTime.Now;
        public DateTime CompletedDate { get; internal set; }
    }
}

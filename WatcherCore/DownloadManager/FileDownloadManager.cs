﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.DownloadManager {
    public class FileDownloadManager : IDisposable {
        private const int checkIntervalMs = 200;
        public int MaxConcurrentDownloads { get; set; } = 2;
        public int TotalFilesDownloaded { get; protected set; }
        public long TotalBytesDownloaded { get; protected set; }
        public int TotalFailures { get; protected set; }
        public List<FileDownloadTransfer> Transfers { get; protected set; } = new List<FileDownloadTransfer>();
        public int TotalRunningDownloads {
            get {
                return Transfers.Where(t => t.State == FileDownloadState.Downloading).Count();
            }
        }
        public int TotalPendingDownloads {
            get {
                return Transfers.Where(t => t.State == FileDownloadState.Pending).Count();
            }
        }
        public int TotalDownloadsCompleted {
            get {
                return Transfers.Where(t => t.State == FileDownloadState.Complete).Count();
            }
        }
        public int TotalDownloadsNotCompleted {
            get {
                return TotalRunningDownloads + TotalPendingDownloads;
            }
        }
        public bool IsCompleted {
            get {
                if (TotalRunningDownloads > 0) {
                    return false;
                }

                if (TotalDownloadsNotCompleted > 0) {
                    return false;
                }

                return true;
            }
        }

        public event EventHandler<FileDownloadStartedEventArgs> FileDownloadStarted;
        public event EventHandler<FileDownloadCompleteEventArgs> FileDownloadCompleted;
        public event EventHandler<FileDownloadSuccessEventArgs> FileDownloadSuccessful;
        public event EventHandler<FileDownloadFailedEventArgs> FileDownloadFailed;

        private Timer processCheck;

        internal FileDownloadManager() {
            processCheck = new Timer(checkIntervalMs);
            processCheck.Elapsed += (o, e) => checkDownloads();
            processCheck.Start();
        }

        public void Download(string downloadUrl, string downloadFilename) {
            Transfers.Add(new FileDownloadTransfer() {
                FileUri = new Uri(downloadUrl),
                FileName = downloadFilename,
                State = FileDownloadState.Pending
            });
        }

        private void checkDownloads() {
            if (TotalPendingDownloads == 0 || TotalRunningDownloads >= MaxConcurrentDownloads) {
                return;
            }

            var nextPending = Transfers
                .Where(t => t.State == FileDownloadState.Pending)
                .OrderBy(t => t.RequestedDate)
                .FirstOrDefault();

            downloadFile(nextPending);
        }

        private void downloadFile(FileDownloadTransfer download) {
            if (download == null) {
                return;
            }

            var client = new WebClient();

            client.DownloadFileCompleted += (o, e) => {
                download.CompletedDate = DateTime.Now;

                if (e.Cancelled || e.Error != null) {
                    download.State = (e.Cancelled) ? FileDownloadState.Cancelled : FileDownloadState.Failed;
                    TotalFailures++;

                    FileDownloadFailed?.Invoke(this, new FileDownloadFailedEventArgs() {
                        RequestUri = download.FileUri.ToString(),
                        Filename = download.FileName,
                        Cancelled = e.Cancelled,
                        Error = e.Error
                    });
                }
                else {
                    download.State = FileDownloadState.Complete;
                    TotalFilesDownloaded++;
                    TotalBytesDownloaded += download.FileSizeBytes;

                    FileDownloadSuccessful?.Invoke(this, new FileDownloadSuccessEventArgs() {
                        RequestUri = download.FileUri.ToString(),
                        Filename = download.FileName,
                        FilesizeBytes = download.FileSizeBytes
                    });
                }

                var ev = new FileDownloadCompleteEventArgs() {
                    Filename = download.FileName,
                    RequestUri = download.FileUri.ToString(),
                    FilesizeBytes = download.FileSizeBytes,
                    Cancelled = e.Cancelled,
                    Error = e.Error
                };

                FileDownloadCompleted?.Invoke(this, ev);

                client.Dispose();
            };

            client.DownloadProgressChanged += (o, e) => {
                download.FileSizeBytes = e.TotalBytesToReceive;
                download.DownloadedBytes = e.BytesReceived;
            };

            client.DownloadFileAsync(download.FileUri, download.FileName);
            download.State = FileDownloadState.Downloading;

            FileDownloadStarted?.Invoke(this, new FileDownloadStartedEventArgs() {
                Filename = download.FileName,
                RequestUri = download.FileUri.ToString(),
            });
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    processCheck.Stop();
                    processCheck.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}

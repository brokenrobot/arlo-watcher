﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.DownloadManager {
    public enum FileDownloadState {
        Pending,
        Downloading,
        Complete,
        Failed,
        Cancelled
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.DownloadManager {
    public class FileDownloadFailedEventArgs : EventArgs {
        public string Filename { get; set; }
        public string RequestUri { get; set; }
        public bool Cancelled { get; set; }
        public Exception Error { get; set; }
    }
}

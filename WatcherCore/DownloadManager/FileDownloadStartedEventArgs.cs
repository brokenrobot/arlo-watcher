﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.DownloadManager {
    public class FileDownloadStartedEventArgs : EventArgs {
        public string Filename { get; set; }
        public string RequestUri { get; set; }
    }
}

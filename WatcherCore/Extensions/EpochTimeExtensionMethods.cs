﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherCore.Extensions {
    /// <summary>
    /// Adds extension methods for handling epoch time conversions.
    /// Epoch timestamp values are stored using the long datatype.
    /// </summary>
    static class EpochTimeExtensionMethods {
        private static readonly DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Converts from an epoch timestamp to a .NET DateTime
        /// </summary>
        /// <param name="epochValue">Epoch timestamp value</param>
        /// <returns>A .NET DateTime value equivalent to the epoch timestamp</returns>
        public static DateTime ConvertFromEpochTime(this long epochValue) {
            return unixEpoch.AddMilliseconds(epochValue).ToLocalTime();
        }

        /// <summary>
        /// Converts from a .NET DateTime to an epoch timestamp value
        /// </summary>
        /// <param name="value">.NET DateTime</param>
        /// <returns>An epoch timestamp value equivalent to the .NET DateTime</returns>
        public static long ConvertToEpochTime(this DateTime value) {
            var diff = value - unixEpoch;
            return (long)diff.TotalMilliseconds;
        }
    }
}

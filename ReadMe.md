# Arlo Watcher version 0.0.1-alpha
A solution that integrates with the Netgear Arlo security system to add additional capabilities. Arlo Watcher is written in C# for .NET 4.6 or above. There is a console application that can be used to automate control of your Arlo system. There is also a class library that can be used to write your own .NET application to integrate with Arlo.

The primary goal of this project is to integrate with the Arlo web service and automatically download recordings from the service. This is currently facilitated by the console application. Future enhancements may require moving away from the console app design.

---

This project is currently in *ALPHA* and **DOES NOT WORK YET**...

---

Planned future features:

 * Ability to read and record the event stream.
 * Increased ability to read camera properties.
 * Increased automation capabilities:
   * Hopefully able to trigger automation based on night vision and/or light level.
   * Automate more properties such as brightness level.
   * Automate switching between security modes.

---

Big thanks to jeffreydwalter for his python project on github: https://github.com/jeffreydwalter/arlo
Much of the REST web service syntax was cribbed from his code. It made building this a lot easier.

---
   
This project uses Newtonsoft Json.NET.
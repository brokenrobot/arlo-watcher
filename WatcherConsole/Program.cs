﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EonDream.ArloWatcher.WatcherConsole {
    class Program {
        static int Main(string[] args) {
            int exitCode = 0;

            Console.Write("Press enter to begin...");
            Console.ReadLine();

            exitCode = MainAsync(args).GetAwaiter().GetResult();

            Console.Write("Press enter to exit...");
            Console.ReadLine();

            return exitCode;
        }

        static async Task<int> MainAsync(string[] args) {
            Console.WriteLine("Creating and configuring arlo watcher console..");
            using (var arloCon = new ArloConsole()) {
                if (await arloCon.Login()) {

                    //await arloCon.ListRecordings(new DateTime(2017, 7, 1));

                    //await arloCon.DownloadDaysAsync(new DateTime(2017, 4, 1), new DateTime(2017, 8, 1));

                    await arloCon.DownloadAllVideos();

                    //Console.Write("Press enter to logout...");
                    //Console.ReadLine();

                    await arloCon.Logout();
                    return 0;
                }
                else {
                    return 1;
                }
            }
        }
    }
}

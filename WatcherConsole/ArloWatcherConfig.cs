﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EonDream.ArloWatcher.WatcherConsole {
    class ArloWatcherConfig {
        private const string OPTION_NAME_ARLO_USER = "ARLO_USERNAME";
        private const string OPTION_NAME_ARLO_PASS = "ARLO_PASSWORD";
        private const string OPTION_NAME_DOWNLOAD_FOLDER = "DOWNLOAD_FOLDER";
        private const string OPTION_NAME_DOWNLOAD_LIMIT = "DOWNLOAD_LIMIT";
        private const string OPTION_NAME_DOWNLOAD_THUMBNAILS = "DOWNLOAD_THUMBNAILS";
        private const string OPTION_NAME_DOWNLOAD_THEN_DELETE = "DOWNLOAD_THEN_DELETE";
        private const string OPTION_NAME_LOG_FOLDER = "LOG_FOLDER";

        private readonly Dictionary<string, string> optionDefaults = new Dictionary<string, string>() {
            { OPTION_NAME_DOWNLOAD_LIMIT, "5" },
            { OPTION_NAME_DOWNLOAD_THUMBNAILS, "true" },
            { OPTION_NAME_DOWNLOAD_THEN_DELETE, "false" },
            { OPTION_NAME_DOWNLOAD_FOLDER, $"{ AppDomain.CurrentDomain.BaseDirectory }\\logs" },
            { OPTION_NAME_LOG_FOLDER, $"{ AppDomain.CurrentDomain.BaseDirectory }\\downloads" }
        };

        public string ArloUsername {
            get {
                return ReadOptionValue(OPTION_NAME_ARLO_USER);
            }
        }

        public string ArloPassword {
            get {
                return ReadOptionValue(OPTION_NAME_ARLO_PASS);
            }
        }

        public string DownloadFolder {
            get {
                return ReadOptionValueFilepath(OPTION_NAME_DOWNLOAD_FOLDER);
            }
        }

        public int DownloadLimit {
            get {
                return ReadOptionValueInt32(OPTION_NAME_DOWNLOAD_LIMIT);
            }
        }

        public bool DownloadThumbnails {
            get {
                return ReadOptionValueBoolean(OPTION_NAME_DOWNLOAD_THUMBNAILS);
            }
        }

        public bool DownloadThenDelete {
            get {
                return ReadOptionValueBoolean(OPTION_NAME_DOWNLOAD_THEN_DELETE);
            }
        }

        public string LogFolder {
            get {
                return ReadOptionValueFilepath(OPTION_NAME_LOG_FOLDER);
            }
        }

        private string ReadOptionValue(string optionName) {
            var hasDefault = optionDefaults.ContainsKey(optionName);
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(optionName) && !hasDefault) {
                throw new ConfigurationErrorsException($"The required option '{ optionName }' is not found in the application configuration file.");
            }

            var val = ConfigurationManager.AppSettings[optionName];

            if (string.IsNullOrEmpty(val)) {
                if (hasDefault) {
                    return optionDefaults[optionName];
                }
                else {
                    throw new ConfigurationErrorsException($"The required option '{ optionName }' has no value set in the application configuration file.");
                }
            }
            else {
                return val;
            }
        }

        private string ReadOptionValueFilepath(string optionName) {
            var val = ReadOptionValue(optionName);
            if (val.EndsWith("\\")) {
                return val;
            }
            else {
                return string.Concat(val, "\\");
            }
        }

        private int ReadOptionValueInt32(string optionName) {
            var val = ReadOptionValue(optionName);
            int tryInt = 0;
            if (int.TryParse(val, out tryInt)) {
                return tryInt;
            }
            else {
                throw new ConfigurationErrorsException($"The option '{ optionName }' has an invalid value of '{ val }' when an integer is expected.");
            }
        }

        private bool ReadOptionValueBoolean(string optionName) {
            var val = ReadOptionValue(optionName);

            string[] yesValues = { "y", "yes", "t", "true", "1" };
            string[] noValues = { "n", "no", "f", "false", "0" };

            if (yesValues.Where(y => y.Equals(val, StringComparison.OrdinalIgnoreCase)).Count() > 0) {
                return true;
            }
            else if (noValues.Where(n => n.Equals(val, StringComparison.OrdinalIgnoreCase)).Count() > 0) {
                return false;
            }
            else {
                throw new ConfigurationErrorsException($"The option '{ optionName }' has an invalid value of '{ val }' when a boolean is expected.");
            }
        }
    }
}

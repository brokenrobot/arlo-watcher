﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EonDream.ArloWatcher.WatcherCore.Api.Client;
using EonDream.ArloWatcher.WatcherCore.Automations;

namespace EonDream.ArloWatcher.WatcherConsole {
    class ArloConsole : IDisposable {
        private ArloClient apiClient;
        private ArloWatcherConfig config;
        private AutomaticDownloader autoDl;
        private System.IO.StreamWriter logfile;

        public ArloConsole() {
            config = new ArloWatcherConfig();

            if (!System.IO.Directory.Exists(config.LogFolder)) {
                System.IO.Directory.CreateDirectory(config.LogFolder);
            }

            logfile = new System.IO.StreamWriter($"{ config.LogFolder }{ DateTime.Now.ToString("yyyyMMddHHmmssfff") }.log");

            apiClient = new ArloClient();
            apiClient.Api.ApiHttpError += (o, e) => {
                Write($"API ERROR! HTTP STATUS { (int)e.StatusCode } { e.StatusCode } - {e.ErrorMessage } - { e.RequestUrl }");
            };

            apiClient.Api.SendingRequest += (o, e) => {
                Write($"Sending { e.RequestType } request to arlo api at { e.RequestUri }");
            };

            apiClient.Api.ReceivedResponse += (o, e) => {
                Write($"Received response from arlo api at { e.RequestUrl } :: ({ ((int)e.StatusCode) } - { e.StatusCode })");
            };
            
            autoDl = new AutomaticDownloader(config.DownloadFolder, apiClient);
            autoDl.Downloads.MaxConcurrentDownloads = config.DownloadLimit;
            autoDl.DownloadThumbnails = config.DownloadThumbnails;
            autoDl.DeleteRecordingsFromArlo = config.DownloadThenDelete;

            autoDl.Downloads.FileDownloadFailed += (o, e) => {
                if (e.Cancelled) {
                    Write($"File download cancelled - {e.Filename }");
                }
                else {
                    Write($"File download failed - { e.Filename } : { e.Error.Message }");
                }
            };
            autoDl.Downloads.FileDownloadCompleted += (o, e) => {
                Write($"File download succeeded - { e.Filename }");
            };
        }

        public async Task<bool> Login() {
            if (await apiClient.LoginAsync(config.ArloUsername, config.ArloPassword)) {
                Write($"Login successful. Email: { apiClient.User.Email }; User ID: { apiClient.User.UserId }");
                return true;
            }
            else {
                Write("Login failed.");
                return false;
            }
        }

        public async Task Logout() {
            Write($"Logging out. Email: { apiClient.User.Email }; User ID: { apiClient.User.UserId }");
            await apiClient.LogoutAsync();
        }

        public async Task ListDevices() {
            Write("Reading device list...");
            var devs = await apiClient.GetDevicesAsync();
            int i = 1;
            foreach (var d in devs) {
                Write($"Device { i++ } - { d.DeviceName } - { d.DeviceType } - { d.ModelId } - { d.LastModified }");
            }
        }

        public async Task DownloadAllVideos() {
            Write($"Beginning download of all recordings...");
            await autoDl.StartAutomaticDownload();
            ShowDownloadStatus();
        }

        public async Task DownloadDaysAsync(DateTime fromDate, DateTime toDate) {
            Write($"Searching for recordings on all days from { fromDate.ToShortDateString() } to { toDate.ToShortDateString() }");
            //autoDl.DeleteRecordingsFromArlo = false;
            for (var d = fromDate; (toDate - d).TotalDays >= 0; d = d.AddDays(1)) {
                Write($"Searching for vidoes on { d.ToShortDateString() }...");
                await ListRecordings(d);
                await autoDl.DownloadRecordings(d);
            }
            ShowDownloadStatus();
        }

        public async Task ListRecordings(DateTime searchDate) {
            Write($"Listing recordings from { searchDate.ToString("d MMM yyyy") }...");
            var recs = await apiClient.GetRecordingsAsync(searchDate);
            int i = 1;
            foreach (var r in recs) {
                Write($"Recording { i++ } - { r.UniqueId } - { r.CreatedAt } - { r.CurrentState } - { r.ContentType } - { r.MediaDurationLabel } - { r.Reason }");
            }
        }

        private void ShowDownloadStatus() {
            while (!autoDl.IsCompleted) {
                Write($"Download manager status: { autoDl.Downloads.TotalRunningDownloads } running. { autoDl.Downloads.TotalPendingDownloads } pending. { autoDl.Downloads.TotalFailures } failures. Downloaded { autoDl.Downloads.TotalFilesDownloaded } files in { autoDl.Downloads.TotalBytesDownloaded } bytes.");
                int x = 1;
                foreach (var t in autoDl.Downloads.Transfers.OrderBy(t => t.RequestedDate)) {
                    if (t.State == WatcherCore.DownloadManager.FileDownloadState.Downloading) {
                        Write($"\tTransfer #{ x } - { t.State } :: { t.FileName } :: { t.DownloadedBytes } of { t.FileSizeBytes } - { t.DownloadedPercent }% complete.");
                    }
                    x++;
                }
                System.Threading.Thread.Sleep(1000);
                Write("\n\n");
            }
        }

        /*

        public async Task ListPlans() {
            Write("Reading plan list...");
            var plans = await apiClient.GetPlansAsync();
            int i = 1;
            foreach(var p in plans) {
                Write($"Plan { i++ } - { p.PlanName } - { p.Preferences.Storage.AutoDelete } - { p.Preferences.Alerts.LowBatteryAlert } - { p.PlanId }");
            }
        }
        */
        private void Write(string message) {

            Console.WriteLine(message);
            logfile?.WriteLine(message);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing) {
            if (!disposedValue) {
                if (disposing) {
                    apiClient?.Dispose();
                    logfile?.Flush();
                    logfile?.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose() {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
